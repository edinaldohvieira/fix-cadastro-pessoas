=== Fix Cadastro de Pessoas ===
Contributors: edinaldohvieira
Donate link: https://example.com/
Tags: comments, spam
Requires at least: 4.5
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Permite o cadastro de nomes e endereços


== Descrição ==

Nomes e endereços. Este pequeno sistema em formato de plugin do WordPress permite que qualquer pessoa possa efetuar um ou vários cadastros do tipo pessoas, ou seja, nomes e endereços, de modo que cada cadastro passa a fazer parte de uma listagem denominada Custom Post Type Pessoas ou simplemente "pessoas"

Pode ser usado para salvar consultar uma lista de contatos, talvez uma lista de prospectos (possiveis novos clientes) ou até mesmo para salvar uma lista de associados para posterormente imprimir as carteirinhas dos associados.


== Perguntas Frequentes ==

= Quanto custa este sistema? =

Zero. Esta é a versão básica do plugin e você pode baixar e instalar na sua instalação do WordPress. Portanto, não está previsto custo algum, a menos que você planeje alterações no plugin (para por exemplo, atender sua necessidade especifica, aí sim você, opcionalmente, pode querer contratar o desenvolvedor para efetuar tais modificações ou inclementações em uma versão melhorada deste plugin.


== Screenshots ==


== Changelog ==

= 0.1.0 =
* Inicio.



